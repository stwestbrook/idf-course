# Operator Overloading

---

* Operators can be overloaded as well, so you can more easily work with user-defined types. 
* Overload operators are basically functions named **operator**, followed by a symbol.
* This example demonstrates adding two Foo objects together with the + operator, and returns a Foo object. 

```cpp
// If you declare the operator as a member of class Foo
Foo operator+(const Foo&);

// Otherwise
Foo operator+(const Foo& a, const Foo& b) {
    Foo c;
    c.number = a.number + b.number;
    return c;
}

int main() {
    Foo foo1, foo2;
    //do stuff
    Foo foo3 = foo1 + foo2;
    //continue
}
```

## What operators can be overloaded?

#### The following operators can be overloaded:

#### ![](/assets/Capture.PNG)The following operators cannot be overloaded:

#### ![](/assets/capture2.PNG)



