# Final

---

* If we mark a function with **final**, then we cannot override it in a derived class.
* If we mark an entire class as **final**, then the class cannot be inherited from. 
* This is new to **C++11**

### Let's take a look at some example code...

```cpp
// Stub code, not meant to compile

// Making functions as final
class Logger {
public:
    ~Logger() { }
    void info(const char message[]) final;
    void error(const char message[]) final;
};

// Marking class as final
class Logger final {
protected:
    char message[];
...
};
```

* What happens to a class's **protected **members, if the class is marked as **final?**



