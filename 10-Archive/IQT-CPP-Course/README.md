# IQT C++ Programming

---

![](/assets/cpp.png)

## Chapters:

* #### Ch1: Intro to C++
* #### Ch2: The C++ STL
* #### Ch3: OOP
* #### Ch4: Overloading and Templates
* #### Ch5: Resource Management

## Resources:

#### [Documentation](http://www.cplusplus.com/doc/tutorial/)

#### [Language Reference](http://en.cppreference.com/w/)

#### [Gitlab Repo](https://gitlab.com/wstaud/CPP-Programing-2018)



