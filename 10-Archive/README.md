# 08-Archive

**The purpose of the Archive is to provide a location for topics that have been modified or have been removed from the current training program.  Version control is important to our course program and development efforts.  It allows us to recover lost topic, should they be requested in the future, and learn from previous efforts.**  
