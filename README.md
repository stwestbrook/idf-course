# Initial Developer Fundementals Course (IDF) README

The purpose of IDF is to provide a foundation for individuals seeking to become software developers. Most of the requriements for inclusion into this course were taken from Official Authorities, and Joint military subject matter experts (SME). A full list of requirements for the Basic Developer can be found on the 90COS [Master Training Task List](https://gitlab.com/90COS/public/mttl/blob/master/Basic%20Dev.md).  

**Note** Not all of the Basic Developer Requirements are taught in the IDF course.  This is a full requirements list of which some are fulfilled in alternate locations.

IDF was created with the notion that it would be taught in a classroom environment with an instrutor familiar with this curriculum to provide additional knowledge, exercises, and fill curriculum gaps.


## Getting Started

### Hardware Requirements
This course requires access to a computer with at minimum the following attributes
* Windows with minimum: intel i5 proc, 500GB HD, 16 gb ram.
* linux Similar requirements  

### Internet
Access to open internet.  This is used to for the following:
* Curriculum access for the IDF course
* Communication between students/instructor
* Receive and turn-in assignments
* Research & study

### Software Requirments include
This training takes into consideration the use of programming tools. This list comprises required software for this course.

**Windows**
* Visual Studio Community 2017
* Atom, Visual Studio Code or Sublime Text
* MinGW
* Open Command Prompt
* CMake
* IDA Pro
* WinDBG
* NASM
* Wire Shark
* Python

**Linux**
Fedora, Ubuntu, or Kali etc...

* GCC
* G++
* GDB
* NASM
* CMAKE
* Python 2.7
* Python 3.x
* Slack
* Netcat
* Wireshark
* Text editor
* Developer Tools
* VmWare or VirtualBox  

The [Equipment Setup](https://gitlab.com/90COS/public/idf-course/blob/master/02-Equipment-Setup/00-Table-of-Contents.md) portion of training contains this list of software requirements and provides some setup guidance.

## Course Access

The IDF course may be accessed via 90 Cos GitLab [IDF Course Introduction](https://gitlab.com/90COS/public/idf-course/tree/master/01-IDF-Course-Introduction).  Please read the course introduction for guidance on navigating the course curriculum.
