## Cyber A-La-Carte

This Repository provides links to all of the the Cyber Training created within the CyberTrainingUSAF Project.  The **A La Carte** is specifically used for training on a stand-alone basis.

**Links to Navigate the course Curriculum**

Please use the Table of Contents (TOC) and the continue to next topic links to navigate the curriculum. Each curriculum subject is tied to our introduction page which may be accessed from any of our curriculums TOC's. By clicking on the name of the subject (below) you will be taken to the associated TOC. 

**WARNING** If you use the course links at the top of the github page (example: CyberTrainingUSAF/03-Introduction-to-Git), to navigate; you will be thrown into the curriculum folder. This can be confusing to navigate from.  If this happens, you may scroll down on the page and click return to TOC.

**The following, is a list of accessable Training.  Click on the name of the training you want, in order to access it's Table of Contents (TOC).  From the TOC you will have access to all of the associated curriculum and labs.:**


* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/09-Assembly/00-Table-of-Contents.md" > Assembly </a>

* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/08-C-Programming/00-Table-of-Contents.md" > C Programming </a>

* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/10-Archive/IQT-CPP_Programming/00-Table-of-Contents.md" > C++ Programming </a>

* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/03-Introduction-to-Git/00-Table-of-Contents.md" > Introduction to Git </a>


* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/07-Network-Programming/00-Table-of-Contents.md" > Networking Concepts </a>

* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/04-Logic-Pseudocode_IDEs/00-Table-of-Contents.md" > Pseudocode & Intro to debugging </a>

* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/Powershell_Training/00-Table-of-Contents.md" > PowerShell </a>

* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/05-Python-Programming/00-Table-of-Contents.md" > Python Programming </a>

* <a href="https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/06-Intro-to-Algorithms/00-Table-of-Contents.md" > Python Algorithms </a>


---

## Course Information

Much of the Training associated with this projected was designed with two things in mind.  First is that the individual recieving the training is willing to research and activily seek out knowledge.  Second is that portions of this training were meant to have an instructor present, so if it appears incomplete you may need to research for the answers to your questions.  There are many free online services, and chatrooms that can be used to help you with questions, if you are not part of our training program.  

