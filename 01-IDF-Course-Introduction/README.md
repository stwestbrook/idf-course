# Course-Introduction
Welcome to the 90th COS Initial Developer Fundamental (IDF) training. This is a ~16 week developer course designed to provide you with the basic knowledge and skills required to enable you to perform as a new programming developer. If this is your first time programming or you have very little experience, this course will be challenging. If you have programming experience, the instructors will provide more robust assignments to help improve your skillset.

### Links to Navigate the course Curriculum
Please use the Table of Contents (TOC) and the continue to next topic links to navigate the curriculum. Each curriculum subject is tied to our introduction page which may be accessed from any of our curriculums TOC's. By clicking on the name of the subject (below) you will be taken to the associated TOC. 

**WARNING** If you use the course links at the top of the github page (example: CyberTraining/02-Equipment-Setup), to navigate, you will be thrown into the curriculum folder. If this happens, you may scroll down on the page and click return to TOC.

### Use the following links to access the associated curriculum TOC:

[Equipment Setup](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/02-Equipment-Setup/00-Table-of-Contents.md)

[Introduction to Git](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/03-Introduction-to-Git/00-Table-of-Contents.md)

[Pseudocode, debugging](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/04-Logic-Pseudocode_IDEs/00-Table-of-Contents.md)

[Python Programming](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/05-Python-Programming/00-Table-of-Contents.md)

[Intro to Algorithms](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/06-Intro-to-Algorithms/00-Table-of-Contents.md)

[Intro to Network programming](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/07-Network-Programming/00-Table-of-Contents.md)

[C Programming](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/08-C-Programming/00-Table-of-Contents.md)

[Assembly](https://gitlab.com/-/ide/project/90COS/public/idf-course/blob/master/-/09-Assembly/00-Table-of-Contents.md)

## Course Information

The IDF course starts off with a review of the tools and equipment the students will have available to them throughout the training.  The course then steps into teaching pseudocode, followed by an introduction to software development concepts using GIT, as well as debugging.  

**Note:**   Introductions to Git and Debugging will be taught intitially at the start of this course, and used throughout its entirety.  Each course participant will be required to conduct daily uploads to their personal GIT repository and all classwork will be debugged.

After this introductory training, the next topics include Python and Python algorithms.  Python is a very forgiving language and transitions well from pseudocode. This course teaches Python 3.X.  We do provide references for conversion to Python 2.7.  Throughout Python, students are expected to read and understand documentation, look up modules, code, and be fully aware of how to access and use the Python library, etc... 

Networking concepts follow Python in the training rotation.  The student will learn basic networking and will be required to perform socket programming using python at this point in the training.

Following a short break in training, the course enters into C programming.  Traditionally ‘C’ is the more difficult of the languages we teach, since it is a very unforgiving language.  Due to its difficulty we teach it at a much slower pace to provide students with less programming experience, an opportunity to grasp the syntax.  It is imperative that students learn C programming due to the amount of work at the squadron that will require them to have a firm grasp on it.  

Next, is Assembly which is used primarily for direct hardware manipulation, access to specialized processor instructions, or to address critical performance issues.  This combined with C, gives the student a better whole picture understanding of how the computer works. The student will be directly working with memory, registers, etc. 

The project portion of the IDF training provides the student with hand on software development exposure.  Starting with pseudo code, Version Control, debugging, and moving on to code development.

**Note** Instead of the traditional Powerpoint setup; we use GitLab to navigate the course. The entire course is written using markdown language. It is tracked via the git platform and displayed in GitLab. The course is easy to navigate and well organized, so there is nothing to worry about. 
