<a href="https://github.com/CyberTrainingUSAF/02-Equiptment-Setup/blob/master/00-Table-of-Contents.md" rel="Return to TOC"> Return to TOC </a>

# 02-Equipment-Setup

This topic provides the required list of equipment tools for use in this course.  These tools are shown based on the Operating system (OS) they use.(Linux, Windows)

---

<a href="https://github.com/CyberTrainingUSAF/02-Equiptment-Setup/blob/master/01_Linux.md" rel="Continue to Next Topic"> Continue to Next Topic </a>

