<a href="https://github.com/CyberTrainingUSAF/02-Equiptment-Setup/blob/master/00-Table-of-Contents.md" rel="Return to TOC"> Return to TOC </a>

# Windows Setup

Setting up tools on Windows can be somewhat challenging. The laptops you are using, have been flashed and setup prior to your arrival, so everything should be good. So instead of going through how to install each program, we are just going to verify all software and correct of modify any inconsistancies.

## Software

#### Visual Studio Community 2017

1. Click Windows button
2. Type in "visual studio" and Visual Studio Community 2017 should pop up. 
3. If it did, follow steps below to verify C environment 
   1. Sign in/register if asked \(You will need an email address\)
   2. Click **file**-&gt;**new**-&gt;**project**
   3. Ensure **Visual C++** is present in the left sidebar, click it
   4. Ensure **Windows Console Application** and **Empty Project** appear in main window

#### Atom, Visual Studio Code or Sublime Text

1. Click Windows button
2. Type in "atom" and Atom should appear, etc

#### MinGW

1. Open **File Explorer**, the folder icon on taskbar
2. Click **This PC** in the left hand side of window
3. Click on the **C** drive
4. A folder called **MinGW** should appear
5. If it does:
   1. Open **Command Prompt**
   2. Type: **gcc** --press enter
   3. An error should appear "gcc: xxxxxx"
   4. Type: **g++** --press enter
   5. An error should appear "g++: xxxxxx"
   6. Type: **gdb** --press enter
   7. You should see: **\(gdb\)**
   8. Type: **quit** --press enter

#### CMake

1. Click Windows button
2. Type in "cmake" and CMake should appear

#### IDA Pro

1. Verify IDA Pro exists using same method as CMake

#### WinDBG

1. Ensure WinDBG exists using same method as CMake

#### NASM

1. Ensure NASM exists using same method as CMake

#### Wire Shark

1. Ensure Wire Shark exists using same method as CMake

#### Python

1. Open **Command Prompt**
2. Type in **py -2** and press enter
3. **&gt;&gt;&gt;** should appear \(Verify Python 2.x\)
4. Press ctrl-C
5. Type in **py -3**
6. **&gt;&gt;&gt;** should appear \(Verify Python 3.x\)
7. Press ctrl-C

---
**End of Equipment Setup**

<a href="https://github.com/CyberTrainingUSAF/03-Introduction-to-Git/blob/master/00-Table-of-Contents.md" > Continue to Introduction to Git </a>

<a href="https://github.com/CyberTrainingUSAF/01-Course-Introduction-and-setup/blob/master/README.md" rel="Return to Course Introduction"> Return to Course introduction </a>
