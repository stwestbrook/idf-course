Parts of this subject are still **under construction**. We will be adding additional tools/ Tool setup as well as a more thorough introduction

# Equipment Setup - Table of contents

<a href="https://github.com/CyberTrainingUSAF/01-Course-Introduction-and-setup/blob/master/README.md" rel="Return to Course Introduction"> Return to Course introduction </a>

* [Introduction](README.md)
* [Linux](01_Linux.md)
* [Windows](02_Windows.md)
